// Load required modules
var http    = require("http");              // http server core module
var express = require("express");           // web framework external module
var io      = require("socket.io")(http);         // web socket external module
var easyrtc = require("easyrtc");           // EasyRTC external module

var users = {},
	clients = {},
	storedPublicMSG = [];

var app = express();
app.use(express.static(__dirname));

// Start Express http server on port 8080
var webServer = http.createServer(app).listen(8888);

// Start Socket.io so it attaches itself to Express server
var socketServer = io.listen(webServer, {"log level":1});

// Start EasyRTC server
var rtc = easyrtc.listen(app, socketServer);

console.log(rtc);

io.on('connection', function(socket) {

	storedPublicMSG.forEach(function(data) {
		io.to(socket.id).emit('message', data);
	}, this);

	socket.on('disconnect', function() {
		delete users[socket.userName];

		io.sockets.emit('updateUsers', users);

		delete clients[socket.userName];
	});

	// Receives message
	socket.on('addUser', function(userData) {

		var userName = userData.userName;

		// we store the username in the socket session for this client
		users[userName] = {userName: userName, peerID: userData.peerID};

		socket.userName = userName;
		socket.peerID = userData.peerID;

		clients[userName] = socket;

		socket.join('public');

		// echo globally (all clients) that a person has connected
		io.to('public').emit('message', {
			room: 'public',
			message: userName + ' has connected'
		});

		// update the list of users in chat, client-side
		io.sockets.emit('updateUsers', users);
	});

	// Receives message
	socket.on('message', function(data) {
		if (data.room) {
			var data = {
				room: data.room,
				from: socket.userName,
				message: data.message
			};

			storedPublicMSG.push(data);

			socket.broadcast.to(data.room).emit('message', data);
		}
		else
		if (data.toUser) {
			socket.broadcast.to(clients[data.toUser].id).emit('message', {
				fromUser: socket.userName,
				from: socket.userName,
				message: data.message
			});
		}
	});

	socket.on('typing', function(data) {
		var receiver = clients[data.toUser];

		if (receiver) {
			socket.broadcast.to(receiver.id).emit('user/' + socket.userName + '/typing', {
				fromUser: socket.userName,
				state: data.state
			})
		}
	});
});