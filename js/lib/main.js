/**
 * Created by Kewin on 29/10/14.
 */


var isActive = true;

window.onfocus = function () {
	isActive = true;

	window.browserTab.clear();
};

window.onblur = function () {
	isActive = false;
};

var vis = (function(){
	var stateKey, eventKey, keys = {
		hidden: "visibilitychange",
		webkitHidden: "webkitvisibilitychange",
		mozHidden: "mozvisibilitychange",
		msHidden: "msvisibilitychange"
	};
	for (stateKey in keys) {
		if (stateKey in document) {
			eventKey = keys[stateKey];
			break;
		}
	}
	return function(c) {
		if (c) document.addEventListener(eventKey, c);
		return !document[stateKey];
	}
})();

var webcamstream = null,
	userName = window.localStorage.getItem('userName');

window.URL = window.URL || window.webkitURL;
navigator.getUserMedia  = navigator.getUserMedia ||
	navigator.webkitGetUserMedia ||
	navigator.mozGetUserMedia ||
	navigator.msGetUserMedia;

if (!userName) {
	userName  = prompt("Please enter your name", "User" + Math.floor((Math.random() * 10000000) + 1));
	window.localStorage.setItem('userName', userName);
}

window.chatBoxes = {};
window.chats = {};

var selfEasyrtcid = "";

/*
function connect() {
	easyrtc.setVideoDims(320,240);
	easyrtc.setRoomOccupantListener(convertListToButtons);

	easyrtc.easyApp("chat", "selfVideo", ["callerVideo"], function(myId) {
		selfEasyrtcid = myId;
	}, function(errorCode, message) {
		easyrtc.showError(errorCode, message);
	});
}


function clearConnectList() {
	var otherClientDiv = document.getElementById("otherClients");
	while (otherClientDiv.hasChildNodes()) {
		otherClientDiv.removeChild(otherClientDiv.lastChild);
	}
}


function convertListToButtons (roomName, data, isPrimary) {
	clearConnectList();
	var otherClientDiv = document.getElementById("otherClients");
	for(var easyrtcid in data) {
		var button = document.createElement("button");
		button.onclick = function(easyrtcid) {
			return function() {
				performCall(easyrtcid);
			};
		}(easyrtcid);

		var label = document.createTextNode(easyrtc.idToName(easyrtcid));
		button.appendChild(label);
		otherClientDiv.appendChild(button);
	}
}


function performCall(otherEasyrtcid) {
	easyrtc.hangupAll();

	var successCB = function() {};
	var failureCB = function() {};
	easyrtc.call(otherEasyrtcid,
		function(easyrtcid) { console.log("completed call to " + easyrtcid);},
		function(errorMessage) { console.log("err:" + errorMessage);},
		function(accepted, bywho) {
			console.log((accepted?"accepted":"rejected")+ " by " + bywho);
		}
	);
}
*/

(function(window, document, $) {
	'use strict';

	//connect();

	if (window.io) {

		window.socket = io();

		easyrtc.setStreamAcceptor( function(callerEasyrtcid, stream) {
			//var video = document.getElementById('caller');
			//easyrtc.setVideoObjectSrc(video, stream);
		});

		easyrtc.setOnStreamClosed( function (callerEasyrtcid) {
			//easyrtc.setVideoObjectSrc(document.getElementById('caller'), "");
		});

		 window.socket.on('connect', function() {

			 window.socket.emit('addUser', {
			 userName: window.localStorage.getItem('userName'),
			 peerID: false
			 });

		 });

		window.socket.on('updateUsers', function(users) {
			console.log('update users');
			var $users = $('#usersContainer ul').empty(),
				html = '';

			for (var user in users) {
				html += '<li data-name="' + user + '" data-peer-id="' + users[user].peerID + '"><img src="img/male.png" height="16">' + user + '<span class="newMessages"></span></li>';
			}

			$users[0].innerHTML = html;

			$users.children().off('click').on('click', function () {

				var peerID = this.getAttribute('data-peer-id'),
					name = this.getAttribute('data-name');

				var chatBox = chatBoxes[name] = Object.create(ChatBox).init({
					toUser: name
				});

				$('#chatBoxesContainer').prepend(chatBox.$el);

				chatBox.animate();

				var call = peer.call(peerID, webcamstream);

				call.on('stream', function (stream) {
					chatBox.cam.setStream(stream);
				});
			});
		});

		window.socket.on('message', function(data) {
			var id 		= data.room || data.fromUser,
				chatBox = window.chatBoxes[id];

			if (!chats[id]) {
				chats[id] = [];
			}

			chats[id].push(data);

			if (chatBox) {
				chatBox._addMessage(data);
			} else {
				var $user = $('#usersContainer').find('li[data-name=' + data.fromUser + '] .newMessages');
				var newCount = parseInt($user.text(), 10) || 0;

				$user.text(newCount + 1).show();
			}
		})

	}

	var chatBox = chatBoxes['public'] = Object.create(ChatBox).init({
		room: 'public'
	});

	chatBox.$el.prependTo('#chatBoxesContainer');

	chatBox.animate();

	easyrtc.initMediaSource(function() {
		var videoBox = new VideoBox(),
			videoEl  = videoBox.$el.find('video')[0];

		$('#usersContainer').prepend(videoBox.$el);

		videoBox.mute(true);

		easyrtc.setVideoObjectSrc(videoEl, easyrtc.getLocalStream());
		easyrtc.connect("Company_Chat_Line", function() {
			console.log('success');
		}, function() {
			console.log('fail');
		});
	});

})(window, document, $);