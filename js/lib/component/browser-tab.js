/**
 * Created by Kewin on 01/11/14.
 */

(function(window, document) {
	'use strict';

	var counter = 0,
		oTitle = document.title;

	window.browserTab = {
		setNewMessages: function(count) {
			if ((counter += count) > 0) {
				document.title = '(' + counter + ') Nieuwe berichten';
			} else {
				document.title = oTitle;
			}
		},
		clear: function() {
			counter = 0;
			document.title = oTitle;
		}
	}
})(window, document);