window.ChatBox = (function(window, document, $, undefined) {
	'use strict';

	return Object.create(null, {
		unread: {
			writable: true,
			value: 0
		},
		$el: {
			writable: true,
			value: null
		},

		init: {
			value: function(options) {
				var self = this;

				this.options = arguments[0];

				this._create()._bindInput().setVideo();

				if (window.socket) {

					if (this.options.toUser) {
						window.socket.on('user/' + this.options.toUser + '/typing', this.toggleTyping.bind(this));
					}

				}

				return this;
			}
		},

		_create: {
			value: function() {
				this.$el = $($('[data-id=chatBoxTemplate]').html());

				// Set box title.
				this.$el.find('.chatBoxName').text(this.options.room || this.options.toUser);

				var self = this;

				var $closeBtn = this.$el.find('.close'),
					cachedChats = window.chats[this.options.room || this.options.toUser];

				this._addMessage(cachedChats);

				if (this.options.room === 'public') {
					$closeBtn.remove();
				} else {
					$closeBtn.click(function() {
						self.destroy();
					});
				}

				return this;
			}
		},

		animate: {
			value: function() {
				var self = this;

				window.requestAnimationFrame(function() {
					self.$el.addClass("animate");
				});
			}
		},

		_bindInput: {
			value: function() {
				var isTyping = false,
					self = this,
					timeout = null;

				this.$el.find('input').keyup(function(e) {

					if (window.socket && self.options.room !== 'public') {
						window.clearTimeout(timeout);

						timeout = window.setTimeout(function () {
							isTyping = false;
							socket.emit('typing', {toUser: self.options.toUser, state: false});
						}, 1000);

						if (!isTyping) {
							isTyping = true;
							window.socket.emit('typing', {toUser: self.options.toUser, state: true});
						}
					}

					if (e.which !== 13 || !this.value.trim().length)
						return;

					self._post(this.value);

					this.value = '';
				});

				return this;
			}
		},

		toggleTyping: {
			value: function(data) {
				if (data.state) {
					this.$el.find('.isTypingBox').show().text(data.fromUser + ' is typing...');
				} else {
					this.$el.find('.isTypingBox').hide().text('');
				}

				return this;
			}
		},

		setVideo: {
			value: function() {
				this.cam  = new VideoBox({muted: true});

				$('.ownWebCamBox', this.$el).append(this.cam.$el);

				return this;
			}
		},

		_post: {
			value: function(message) {
				this._addMessage({
					from: 'me',
					room: this.options.room,
					message: message
				});

				socket.emit('message', {
					toUser: this.options.toUser,
					room: this.options.room,
					message: message
				});
			}
		},

		setEmoticons: {
			value: function(string) {
				//string = string.replace(':plop', '<audio src="sound/pop.mp3" autoplay autostart></audio>');

				var emotes = [
					[':\\)', 'happy'],
					[':d', 'veryHappy'],
					[':\\(', 'sad'],
					[':\\@', 'angry'],
					[':p', 'tong'],
					[';\\)', 'blink'],
					[':O', 'openMouth'],
					['<3', 'love'],
					['\\(l\\)', 'love'],
					['\\(k\\)', 'kiss'],
					['\\(y\\)', 'thumb'],
					['\\(a\\)', 'angel'],
					['xd', 'crossSmile'],
					[':\'\\(', 'crying']
				];

				for(var i = 0; i < emotes.length; i++) {
					string = string.replace(new RegExp(emotes[i][0], 'gi'), '</span><span class="smiley ' + emotes[i][1] + '" /><span>');
				}

				return string;
			}
		},

		_addMessage: {
			value: function(data) {
				if (!data) {
					return this;
				}

				if (!data.length) {
					data = [data];
				}

				for (var i = 0, len = data.length; i < len; ++i) {
					var msg = data[i],
						className = 'server',
						$from = $('<span />');

					if (msg.from) {
						if (msg.from === 'me') {
							className = 'self';
						} else {
							className = 'default';
						}
						$from.text(msg.from + ': ');
					}

					var message = this.setEmoticons(msg.message);

					var html = '<div class="' + className + '">' + $from.html() + '<span>' + message + '</span></div>';

					this.$el.find('.chatBoxMessages').append(html);
				}

				if (!window.isActive) {
					document.getElementById('pop').play();

					this.unread += data.length;

					window.browserTab.setNewMessages(data.length);
				}

				this.scrollToBottom();
			}
		},

		scrollToBottom: {
			value: function() {
				var content = this.$el.find('.chatBoxMessages')[0];
				content.scrollTop = content.scrollHeight;
			}
		},

		destroy: {
			value: function() {
				this.$el.remove();
				this.$el = null;

				delete window.chatBoxes[this.options.room || this.options.toUser];
			}
		}
    });

})(window, document, $);