(function() {
	'use strict';

	var template = $('#tmplVideo').html();

	var VideoBox = function() {
		this.init(arguments[0]);
	};

	VideoBox.prototype = {
		options: {},

		init: function(options) {
			this.options = options || {};

			this._create();
		},

		_create: function() {
			this.$el = $(template);
			this.$video = $('video', this.$el);

			this.options.muted && this.mute();
		},

		setStream: function(stream) {
			this.$video[0].src = window.URL.createObjectURL(stream);

			this.$el.addClass('animate');
		},

		mute: function(state) {
			this.$video.attr('muted', '');

			return this;
		}
	};

	window.VideoBox = VideoBox;
})();
